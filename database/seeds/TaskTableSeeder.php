<?php

use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert(
        [[
            'title' => 'task1',
            'status' => 'done',
            'id'=>'1',
            'created_at' => date('Y,m,d G:i:s'),
            'user_id' => '1' ,
        ],
        [
            'title' => 'task2',
            'status' => 'done',
            'id'=>'2',
            'created_at' => date('Y,m,d G:i:s'),
            'user_id' => '1' ,
        ],
        [
            'title' => 'task3',
            'status' => 'done',
            'id'=>'3',
            'created_at' => date('Y,m,d G:i:s'),
            'user_id' => '1' ,
        ],
        ]);
    }
}
