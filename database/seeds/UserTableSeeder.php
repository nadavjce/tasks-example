<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'as',
                    'email' => 'a@a.com',
                    'id'=>'1',
                    'password'=> Hash::make('12345678'),
                    'created_at' => date('Y,m,d G:i:s'),
                    'role' => 'admin' ,
                ],
            ]);
    }
}
