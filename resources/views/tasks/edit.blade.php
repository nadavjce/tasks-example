@extends('layouts.app')
@section('content')


<h1> Edit task </h1>
<form method = 'post' action = "{{action('TaskController@update', $task->id)}}"  >
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title"> task to update </label>
    <input type = "text" class = "form-control" name = "title" value = "{{$task->title}}">
    <input type = "text" class = "form-control" name = "status" value = "{{$task->status}}">
</div>

<div class = "form-group">
 <input type = "submit" class= "form-control" name="submit" value= "Update">
</div>

</form>






@endsection